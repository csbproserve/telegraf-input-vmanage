import click
from dotenv import load_dotenv
import os
import ipaddress
import json

load_dotenv()

import pynetbox
import requests
import urllib3
urllib3.disable_warnings(urllib3.exceptions.InsecureRequestWarning)

NETBOX_TOKEN = os.getenv("NETBOX_TOKEN")
NETBOX_HOST = os.getenv("NETBOX_HOST")
NETBOX_PORT = os.getenv("NETBOX_PORT")
NETBOX_TAG = os.getenv("NETBOX_TAG")

VMANAGE_HOST = os.getenv("VMANAGE_HOST")
VMANAGE_PORT = os.getenv("VMANAGE_PORT")
VMANAGE_USER = os.getenv("VMANAGE_USER")
VMANAGE_PASS = os.getenv("VMANAGE_PASS")

nb = pynetbox.api(f"https://{NETBOX_HOST}:{NETBOX_PORT}",token=NETBOX_TOKEN)
nb_session = requests.session()
nb_session.verify=False
nb.http_session=nb_session

try:
    vma_session = requests.session()
    vma_session.verify=False

    vma_auth_resp = vma_session.post(
        url=f"https://{VMANAGE_HOST}:{VMANAGE_PORT}/j_security_check",
        data={
            "j_username": VMANAGE_USER,
            "j_password": VMANAGE_PASS
        }
    )
    if vma_auth_resp.status_code == 200 and len(vma_auth_resp.cookies) > 0:
        vma_token_resp = vma_session.get(
            url=f"https://{VMANAGE_HOST}:{VMANAGE_PORT}/dataservice/client/token",
        )
        if vma_token_resp.status_code == 200:
            VMANAGE_TOKEN = vma_token_resp.content
        else:
            print("invalid token")
            exit(code=1)
    else:
        print("invalid VManage Auth")
        exit(code=1)
except Exception as e:
    print("Could not authenticate to VManage")
    exit(code=1)

@click.group()
def cli():
    """An exec input plugin for Telegraf to gather VManage stats"""
    pass

@click.command('gather')
@click.option('--cell-radio/--no-cell-radio', default=False)
@click.option('--cell-radio-measurement-name', default="cellular_stats")
@click.option('--netbox-tag', default=None)
def gather(cell_radio,netbox_tag,cell_radio_measurement_name):
    if netbox_tag is None:
        netbox_tag = NETBOX_TAG
    devices = nb.dcim.devices.filter(tag=netbox_tag)
    
    if cell_radio:
        try:
            for dev in devices:
                dev.full_details()
                dev_int = nb.dcim.interfaces.get(device_id=dev.id,name="system-ip")
                system_ip_cidr = nb.ipam.ip_addresses.get(interface_id=dev_int.id)
                system_ip = str(ipaddress.ip_interface(system_ip_cidr).ip)

                vma_cell_radio_resp = vma_session.get(
                    url=f"https://{VMANAGE_HOST}:{VMANAGE_PORT}/dataservice/device/cellular/radio?deviceId={system_ip}",
                )
                
                if vma_cell_radio_resp.status_code == 200:
                    vma_cell_radio_json = json.loads(vma_cell_radio_resp.content)
                    for cell_int in vma_cell_radio_json["data"]:
                        print(f'{cell_radio_measurement_name},device={dev.name},interface={cell_int["cellular-interface"]} '\
                            + f'radio-tx-channel={cell_int["radio-tx-channel"]},radio-rat-selected="{cell_int["radio-rat-selected"]}",'\
                            + f'bandwidth="{cell_int["bandwidth"]}",radio-rat-preference="{cell_int["radio-rat-preference"]}",'\
                            + f'radio-power-mode="{cell_int["radio-power-mode"]}",technology="{cell_int["technology"]}",'\
                            + f'radio-rsrq={cell_int["radio-rsrq"]},radio-rsrp={cell_int["radio-rsrp"]},'\
                            + f'radio-rx-channel={cell_int["radio-rx-channel"]},radio-rssi={cell_int["radio-rssi"]},'\
                            + f'radio-snr={cell_int["radio-snr"]},radio-band={cell_int["radio-band"]} ' \
                            + f'{cell_int["lastupdated"]*(10**6)}' )
        except pynetbox.core.query.RequestError as e:
            print("Could not connect to Netbox")
            exit(code=1)
            
cli.add_command(gather)        

if __name__ == "__main__":
    cli()