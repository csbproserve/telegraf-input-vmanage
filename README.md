# telegraf-vmanage-cellular

## Installation

1. Clone repo:
```sh
git clone https://gitlab.com/csbproserve/telegraf-input-vmanage.git
```

2. Setup virtual environment and activate if desired:
```sh
cd telegraf-input-vmanage
python3 -m venv venv
source venv/bin/activate
```

3. Install dependencies and console script:
```sh
pip install .
```

## Setup

Either use .env file to setup environment or set variable via docker/systemd/etc

Sample .env:
```text
NETBOX_TOKEN=<insert your token>
NETBOX_HOST=demo.netbox.dev
NETBOX_PORT=443
NETBOX_TAG=vmanage-monitor-cell-radio
VMANAGE_HOST=sandbox-sdwan-2.cisco.com
VMANAGE_PORT=443
VMANAGE_USER=devnetuser
VMANAGE_PASS=RG!_Yw919_83
```

## Usage

1. Define a tag in netbox to determine which devices to query from VManage. In the example variables above, the Netbox Tag would need a slug of "vmanage-monitor-cell-radio"
2. Make sure the device has an interface named "system-ip" with the IP address of the cedge's system-ip attached.
3. Execute from terminal to test:
```sh
telegraf-input-vmanage gather --cell-radio
```
4. Optionally use --netbox-tag to override environment variable.
```sh
telegraf-input-vmanage gather --cell-radio --netbox-tag=my-cellular-monitoring-tag
```
5. Optionally use --cell-radio-measurement-name to override the default measurement name sent to Telegraf (the default is "cellular_stats").
```sh
telegraf-input-vmanage gather --cell-radio --cell-radio-measurement-name=my_measurement_name
```
4. Add as an exec Telegraf input - 
[Telegraf Exec Plugin](https://github.com/influxdata/telegraf/tree/master/plugins/inputs/exec)
