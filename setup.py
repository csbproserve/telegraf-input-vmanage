from setuptools import setup

setup(
    name='telegraf-input-vmanage',
    version='0.1',
    py_modules=['telegraf_input_vmanage'],
    install_requires=[
        'Click',
        'python-dotenv',
        'pynetbox'
    ],
    entry_points='''
        [console_scripts]
        telegraf-input-vmanage=telegraf_input_vmanage:cli
    ''',
)